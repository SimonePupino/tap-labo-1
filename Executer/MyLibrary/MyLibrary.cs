﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyAttribute;

namespace MyLibrary
{
    public class MyLibrary
    {
        [ExecuteMe]
        public void M1() => Console.WriteLine("Hello, i'm method M1");

        [ExecuteMe(5)]
        [ExecuteMe(7)]
        /*[ExecuteMe(21)]*/
        public void M2(int n) => Console.WriteLine("Hello, i'm method M2 with n={0}", n);

        [ExecuteMe("hello", "reflection")]
        public void M3(string s1, string s2)
        {
            Console.WriteLine("Hello, i'm method M3 with s1={0} s2={1}", s1, s2);
        }   
    }
}
