﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAttribute
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class ExecuteMe : Attribute
    {
        public int NumberOne { get; set; }
        public string StringOne { get; set; }
        public string StringTwo { get; set; }

        public ExecuteMe(int number) => NumberOne = number;

        public ExecuteMe(string s1, string s2)
        {
            StringOne = s1;
            StringTwo = s2;
        }
        public ExecuteMe() { }


    }
}
