﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using MyAttribute;
using MyLibrary;
using MyLibrary = MyLibrary.MyLibrary;


namespace Executer
{
    class Executer
    {
        static void Main(string[] args)
        {
            var a = Assembly.LoadFrom(@"MyLibrary.dll");
            foreach (Type currentClass in a.GetTypes())
            foreach (MethodInfo type in currentClass.GetMethods())
            {
                var parameters = type.GetParameters();
                if (type.GetCustomAttributes(typeof(ExecuteMe), false).Length > 0)
                {
                    Console.WriteLine(type.Name);
                    object classIstance = Activator.CreateInstance(currentClass, null);
                    if (parameters.Length == 0)
                        Console.WriteLine(type.Invoke(classIstance, null));
                    else if (parameters.Length == 1)
                    {
                       var attribute = GetAttributExecuteMe<global::MyLibrary.MyLibrary>(type.Name);
                       object [] paramethersMethod = { attribute.NumberOne };
                       Console.WriteLine(type.Invoke(classIstance, paramethersMethod));
                       //Console.WriteLine(attribute.NumberOne);
                    }
                    else if (parameters.Length == 2)
                    {
                        var attribute = GetAttributExecuteMe<global::MyLibrary.MyLibrary>(type.Name);
                        object[] paramethersMethod = { attribute.StringOne, attribute.StringTwo };
                        Console.WriteLine(type.Invoke(classIstance, paramethersMethod));
                    }
                }
                

            }
            Console.WriteLine("Press any key to quit.");
            Console.ReadLine();
        }

        public static ExecuteMe GetAttributExecuteMe<T>(string method)
        {
            try
            {
                return ((ExecuteMe) typeof(T).GetMethod(method).GetCustomAttributes(typeof(ExecuteMe), false)
                    .FirstOrDefault());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
    }
}
